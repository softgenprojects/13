require('module-alias/register');
const express = require('express');
const cors = require('cors');

const app = express();
app.use(express.json());
app.use(cors({
  origin: ['http://localhost:3080', process.env.CORS_FRONTEND_URL, 'http://65.108.219.251:3080/'],
  optionsSuccessStatus: 200,
  credentials: true 
}));

app.get('/', (req, res) => {
  res.send('Hello, World!');
});

const port = process.env.BE_PORT || 8080;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});